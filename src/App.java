public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("JBR2.20");
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2);
        Circle circle3 = new Circle(3,"green");
        Cylinder cylinder1 =new Cylinder();
        Cylinder cylinder2 =new Cylinder(2.5);
        Cylinder cylinder3 =new Cylinder(3.5,"green");
        Cylinder cylinder4 =new Cylinder(3.5,"green",1.5);
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        System.out.println(circle3.toString());
        //System.out.println(cylinder1.getVolume());
        System.out.println(cylinder1.toString());
        System.out.println(cylinder2.toString());
        System.out.println(cylinder3.toString());
        System.out.println(cylinder4.toString());
    }
}
