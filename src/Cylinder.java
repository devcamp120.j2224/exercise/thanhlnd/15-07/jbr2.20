public class Cylinder extends Circle {
    double height = 1;
    public Cylinder(){
        super();// gọi phương thức circle()
    }
    public Cylinder( double height) {
        super();
        this.height = height;
    }
    
    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }
    
    public Cylinder(double radius,String color) {
        super(radius,color);
        this.height = height;
    }
    public Cylinder(double radius, String color, double height) {
        super(radius, color);
        this.height = height;
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    public double getVolume(){
        return super.getArea()*height;
    }
    @Override
    public String toString() {
        return "Cylinder [height=" + height +", color" + super.getColor() + ", radius" + super.getRadius() + ", getVolume " + getVolume() + "]";
    }
    
}
